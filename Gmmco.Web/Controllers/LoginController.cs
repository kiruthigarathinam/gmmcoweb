﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Gmmco.Web.Helpers;
using Gmmco.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Ubiety.Dns.Core;

namespace Gmmco.Web.Controllers
{
    public class LoginController : Controller
    {
        IConfiguration Configuration;

        public LoginController(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> UserLogin(string userName, string password)
        {
            var baseUrl = Configuration["BaseApiUrl"];
            var userLoginViewModel = new UserLoginViewModel
            {
                UserName = userName,
                Password = password
            };


            PostRequest postRequest = new PostRequest
            {
                MethodName = "api/Account/api/V1/Account/Login",
                Value = userLoginViewModel,
            };

            var response = await new Helpers.Request().PostRequest(postRequest, baseUrl);

            Response apiResponse = JsonConvert.DeserializeObject<Response>(response);
            return Json(response);
        }
    }
}