﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gmmco.Web.Models
{
    public class UserLoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
